# README #  
I've tried to play around a bit with CC, in a regular project structure to see how I can implement it in a "real case" scenario.  
You can find the "advanced" :) code for my tests in 
app/home/home_controller.php and in 
app/home/home_view.php   

This is what I did:  
1. created a CC account and setup 2 settings -> a bool and a text one  
2. Implemented the CC client connection code and discovered that on localhost it doesn't connect to CC  
3. moved to a test server here: https://msynapse.com/work/flags/ and it worked, meaning it needs port 80 or 443 to be open or configured in firewall in order to connect to a local environment -> there should be a client app that opens a tunnel for ease of use.  
4. Tested to see what happens when they are switched on or changed as you will see in the code. It works.  
5. I've moved from testing to production, using the keys that are put in app/_config/config.php and the variables are changed as you will see by clicking the links.  
6. I've added the example of the button text where if you toggle the setting in CC, it changes on refresh.  
  
I don't know if you can access my cc account to toggle them around. It's under andreista@gmail.com.  
