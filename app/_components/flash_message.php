<?php if($this->flashMessage->hasMessages()){ ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->flashMessage->display(); ?>
            </div>
        </div>
    </div>
<?php } ?>