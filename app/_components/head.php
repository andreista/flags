<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
    <title>test</title>
    
    <link rel="stylesheet" href="<?php echo $GLOBALS['assets_url']; ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $GLOBALS['assets_url']; ?>css/style.css">
    <link rel="stylesheet" href="<?php echo $GLOBALS['assets_url']; ?>css/montserrat.min.css">
    <link rel="stylesheet" href="<?php echo $GLOBALS['assets_url']; ?>css/andrei.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
</head>

<body class="bg-light d-flex flex-column h-100">
