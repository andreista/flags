<nav class="navbar navbar-expand-lg navbar-dark bg-dark p-1">
    <div class="container-xl">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto flex-nowrap">
                <li class="nav-item">
                    <a class="nav-link btn btn-sm m-1 p-1" aria-current="page" href="<?php echo $GLOBALS['url'] ?>predictions">
                        <span class="fs-16 "><i class="fas fa-award"></i> Top stuff</span>
                    </a>
                </li>
        </div>
    </div>
</nav>
<div class="container-xl">
