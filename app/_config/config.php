<?php
date_default_timezone_set('Europe/London');
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    $GLOBALS['dbhost'] =  '192.168.1.160';
    $GLOBALS['dbuser'] =  'root';
    $GLOBALS['dbpass'] =  '';
    $GLOBALS['dbname'] =  'flags';

    $GLOBALS['url'] =  'http://localhost/flags/';

    error_reporting(1);
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

    ini_set("display_errors", "On");
} else {
    $GLOBALS['dbhost'] =  'localhost';
    $GLOBALS['dbuser'] =  'root';
    $GLOBALS['dbpass'] =  '';
    $GLOBALS['dbname'] =  'flags';

    $GLOBALS['url'] =  'https://msynapse.com/work/flags/';

    error_reporting(1);
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
    ini_set("display_errors", "On");
}

$GLOBALS['assets_url'] =  $GLOBALS['url'].'assets/';
$GLOBALS['templates_dir'] =  'app/_components/';

$GLOBALS['test_flags_sdk_key'] = 'p6rZCOMNukGN5-tv2SzqTw/lPri2l8YME-y-fscx2dYmA';
$GLOBALS['production_flags_sdk_key'] = 'p6rZCOMNukGN5-tv2SzqTw/3HV0JClpsUSF-UqJmQ_e0g';
?>
