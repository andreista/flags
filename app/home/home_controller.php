<?php
class home_controller extends Controller{
    function __construct(){
        parent::__construct();
    }

    function render_view($name = false) {

        if($this->linkArr[1] == '') {
          $keyName = 'production_flags_sdk_key';
        } else {
          $keyName = $this->linkArr[1].'_flags_sdk_key';
        }


        $client = new \ConfigCat\ConfigCatClient($GLOBALS[$keyName]);

        $currentButtonImplementation = $client->getValue("currentButtonImplementation", false);

        $buttonColor = $client->getValue("button_color", false);

        if($currentButtonImplementation == 1)
          $buttonText = 'button value is on';
        else
          $buttonText = 'button value is OFF';

        $this->view->buttonText = $buttonText;
        $this->view->buttonColor = $buttonColor;
        //$isMyFirstFeatureEnabled = $client->getValue("isMyFirstFeatureEnabled", false);
        /*
        var_dump($currentButtonImplementation);
        var_dump($isMyFirstFeatureEnabled);
        /**/
        $this->view->render_view($name);
    }
}
?>
