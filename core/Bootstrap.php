<?php
class Bootstrap{
    function __construct(){
        $link = $this->create_link();

        if($link[0] == '') $link[0] = 'home';

        $path = 'app/'.$link[0].'/'.$link[0].'_controller.php';
        if( !file_exists ($path) ){
            $link[0] = 'error';
            $path = 'app/error/error_controller.php';
        }

        include_once $path;
        $controllerName = $link[0]."_controller";
        $controller = new $controllerName();

        if($this->seo_page){
            $controller->seo_page = $this->seo_page;
            $controller->view->seo_page = $this->seo_page;
            $controller->view->seoLink = $this->seoLink;
        }

        $controller->view->linkArr = $link;
        $controller->linkArr = $link;
        $controller->load_model($link[0]);

        if(isset($link[1])){
            if(method_exists($controller, $link[1])){
                $controller->{$link[1]}();
            }
        }
        if(method_exists($controller, 'render_view')){
            if($GLOBALS['seo'][$seoName]){
                $controller->view->metaTitle = $GLOBALS['seo'][$seoName]['title'];
                $controller->view->metaDesc = $GLOBALS['seo'][$seoName]['desc'];
            }
            if($controller->seo_page){
                $controller->view->metaTitle = $controller->seo_page['meta_title'];
                $controller->view->metaDesc = $controller->seo_page['meta_desc'];
            }

            $controller->view->activeLink = $link[0];
            $controller->render_view($link[0]);
        }
    }

    private function create_link(){
        $link = rtrim( $_GET['link'], '/');
        $link = explode('/', $link);
        return $link;
    }

}
?>
