<?php
require_once 'libs/configcat/vendor/autoload.php';

class Controller{
    function __construct(){
        $this->view = new View();
    }

    function load_model($name){
        foreach (glob("app/_models/*.php") as $filename) {
            $filenameArr = explode("/", $filename);
            include $filename;
            $filenameArr = array_reverse($filenameArr);
            $modelName = str_replace(".php", "", $filenameArr[0]);
            $tableModelName = $modelName."_model";
            $this->$tableModelName = new $modelName();
        }

        $modelName = $name.'_model';
        $path = 'app/'.$name.'/'.$modelName.'.php';
        if(file_exists($path)){
            require $path;
            $this->model = new $modelName();
        }
    }

    function check_login(){
        if($_SESSION['loginArr']['user_id'] == '')
          echo '<script>window.location.href="'.$GLOBALS['url'].'login"</script>';
    }
}
?>
