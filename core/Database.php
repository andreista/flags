<?php
class Database extends PDO{
    protected static $instance;
    function __construct(){
    }

    public static function getInstance() {
        if(empty(self::$instance)) {
            try {
                self::$instance = new PDO('mysql:host='.$GLOBALS['dbhost'].';dbname='.$GLOBALS['dbname'], $GLOBALS['dbuser'], $GLOBALS['dbpass']);
            } catch(PDOException $error) {
                echo $error->getMessage();
            }
        }
        //echo "<pre class='pre'><code style='word-wrap: break-word;'>".__FILE__.":".__LINE__."<br>";print_r(self::$instance);echo"</code></pre>";
        return self::$instance;
    }

    function select($tableName, $arrParams = [], $orderByField = NULL, $orderBy = NULL, $limit = NULL){
        if($limit != NULL)
            $limit = ' LIMIT '.$limit;

        if($orderBy != NULL)
            $orderBy = ' ORDER BY '.$orderByField.' '.$orderBy;

        if( count($arrParams) > 0 ) {
            $where = ' WHERE ';
            foreach($arrParams as $key => $val){
                $where .= $key.' = :'.$key.' AND ';
                $arr[$key] = $val;
            }
            $where = substr($where, 0, -5);
        }
        $query = "SELECT * FROM ".$tableName.$where.$orderBy.$limit;
        return $this->select_query($query, $arr);
    }

    function select_query($query, $arr = []){
        try {
            $db = self::getInstance();
            $sth = $db->prepare($query);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sth->execute($arr);
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    function select_count($tableName, $arrParams = []){
        if( count($arrParams) > 0) {
            $where = ' WHERE ';
            foreach($arrParams as $key => $val){
                $where .= $key.' = :'.$key.' AND ';
                $arr[':'.$key] = $val;
            }
            $where = substr($where, 0, -5);
        }

        $query = "SELECT * FROM ".$tableName.$where;
        $db = self::getInstance();
        $sth = $db->prepare($query);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sth->execute($arr);
        return $sth->rowCount();
    }

    function insert($table, $arr = []){
        foreach($arr as $key => $val){
            $tableCols .= $key.', ';
            $tableVals .= ':'.$key.', ';
            if($val == 'db_now()'){
                $arr1[':'.$key] = "'NOW()'";
            }else{
                $arr1[':'.$key] = $arr[$key];
            }
        }
        $tableCols = substr($tableCols, 0, -2);
        $tableVals = substr($tableVals, 0, -2);
        try {
            $db = self::getInstance();
            $sth = $db->prepare("INSERT INTO ".$table." (".$tableCols.") VALUES (".$tableVals.")");
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sth->execute($arr1);
            return $db->lastInsertId();
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    function update($table, $updateArr = [], $whereArr = []) {
        $query = "UPDATE ".$table." SET ";
        foreach ($updateArr as $key => $value)
        {
            $query .= $key." = :".$key.", ";
        }
        $query = substr($query, 0, -2);
        $query .= " WHERE";
        foreach ($whereArr as $key => $value)
        {
            $query .= " ".$key." = :".$key." AND ";
            $updateArr[$key] = $value;
        }
        $query = substr($query, 0, -5);
        try {
            $db = self::getInstance();
            $sth = $db->prepare($query);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sth->execute($updateArr);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    function delete($table, $whereArr = []){
        $query = "DELETE FROM ".$table;
        $query .= " WHERE";
        foreach ($whereArr as $key => $value)
        {
            $query .= " ".$key." = :".$key." AND ";
            $arr[$key] = $value;
        }
        $query = substr($query, 0, -5);
        try {
            $db = self::getInstance();
            $sth = $db->prepare($query);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sth->execute($arr);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}
?>
