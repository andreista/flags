<?php
class View {
    function __construct(){
        $this->flashMessage = new \Plasticbrain\FlashMessages\FlashMessages();
    }

    function render_error_view($name){
        $path = 'app/'.$name.'/'.$name.'_view.php';
        include $path;
    }

    function render_view($name){
        include $GLOBALS['templates_dir'].'head.php';
        include $GLOBALS['templates_dir'].'nav.php';

        $folderPath = 'app/'.$name.'/'.$name.'_view/index.php';
        $path = 'app/'.$name.'/'.$name.'_view.php';
        if(file_exists($folderPath)){
            include $folderPath;
        }else{
            include $path;
        }
        include $GLOBALS['templates_dir'].'footer.php';
    }

    function render_login_view($name){
        include $GLOBALS['templates_dir'].'head.php';

        $folderPath = 'app/'.$name.'/'.$name.'_view/index.php';
        $path = 'app/'.$name.'/'.$name.'_view.php';
        if(file_exists($folderPath)){
            include $folderPath;
        }else{
            include $path;
        }
        
        include $GLOBALS['templates_dir'].'footer.php';
    }
}
?>
