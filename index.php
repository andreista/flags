<?php
session_start();
include_once 'app/_config/config.php';

include_once 'core/Bootstrap.php';
include_once 'core/Database.php';
include_once 'core/Controller.php';
include_once 'core/View.php';
include_once 'core/Model.php';
include_once 'libs/FlashMessages.php';

$website = new Bootstrap();

/*
require_once 'libs/configcat/vendor/autoload.php';

$client = new \ConfigCat\ConfigCatClient("p6rZCOMNukGN5-tv2SzqTw/3HV0JClpsUSF-UqJmQ_e0g");

$currentButtonImplementation = $client->getValue("currentButtonImplementation", false);
$isMyFirstFeatureEnabled = $client->getValue("isMyFirstFeatureEnabled", false);

var_dump($currentButtonImplementation);
var_dump($isMyFirstFeatureEnabled);
/**/
?>
